// 1) Метод forEach використовується для виконання заданої функції для кожного елемента масиву.
// 2) Щоб очистити масив, можна встановити його довжину на 0 або присвоїти пустий масив.
// 3) Для перевірки, що змінна є масивом, можна використовувати Array.isArray(variable), який повертає true, якщо variable є масивом.

function filterBy(arr, dataType) {
    return arr.filter(item => typeof item !== dataType);
}
  
const data = ['hello', 'world', 23, '23', null];
const filteredData = filterBy(data, 'string');
console.log(filteredData);
  